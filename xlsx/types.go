package xlsx

type CellType string

// CellTypes define data type in section 18.18.11
const (
	BlankCellType         CellType = ""
	BooleanCellType       CellType = "b"
	DateCellType          CellType = "d"
	ErrorCellType         CellType = "e"
	NumberCellType        CellType = "n"
	SharedStringCellType  CellType = "s"
	FormulaStringCellType CellType = "str"
	InlineStringCellType  CellType = "inlineStr"
)
