package xlsx_test

import (
	"errors"
	"io"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/miatel/go/spreadsheet/xlsx"
)

// "bold.xlsx",
// "deleted-sheet.xlsx",
// "ms-1M.xlsx",
// "ms-100k.xlsx",
// "multiple-sheets.xlsx",
// "no-shared-strings.xlsx",
// "small.xlsx",
// "xl-relationship-prefix.xlsx",

var testCases = []struct {
	FileName string
	Result   [][]any
}{
	{
		FileName: "apple-500.xlsx",
		Result:   nil,
	},
	{
		FileName: "libre-500.xlsx",
		Result:   nil,
	},
	{
		FileName: "ms-500.xlsx",
		Result:   nil,
	},
	{
		FileName: "empty-last-rows.xlsx",
		Result:   nil,
	},
	{
		FileName: "empty-some-cells.xlsx",
		Result:   nil,
	},
	{
		FileName: "valid.xlsx",
		Result: [][]any{
			{
				"Miatel", "79310000022", "Hello, World!",
				time.Date(2023, time.April, 5, 18, 0, 0, 86400, time.UTC),
				"Батомонкуев Дондок Бороевич", "Вектор", "ВКонтакте",
			},
			{
				"Hello, World!",
				time.Date(2023, time.April, 6, 18, 0, 0, 86400, time.UTC),
				"Батомонкуев Дондок Бороевич", "ООО Табеан", "Facebook",
			},
			{
				"Miatel", 7.9310000025e+10, "Test message",
				time.Date(2023, time.April, 8, 18, 0, 0, 86400, time.UTC),
				"Батомонкуев Дондок Бороевич", "Вектор", "Mail.RU"},
		},
	},
}

var testFilesFail = []string{
	"shared-strings-with-r-element.xlsx",
	"shared-strings.xlsx",
}

func TestOpenBinary(t *testing.T) {
	//pp, err := os.Create("/tmp/cpu_profile")
	//require.NoError(t, err)
	//defer pp.Close()
	//err = pprof.StartCPUProfile(pp)
	//require.NoError(t, err)
	//defer pprof.StopCPUProfile()

	for _, test := range testCases {
		t.Run(test.FileName, func(t *testing.T) {
			start := time.Now()

			bytes, err := os.ReadFile("../test/xlsx/" + test.FileName)
			require.NoError(t, err)

			doc, err := xlsx.OpenBinary(bytes)
			require.NoError(t, err)

			sheet, err := doc.Sheet(doc.Sheets[0])
			require.NoError(t, err)

			res := make([][]any, 0, 100)
			for {
				row, err := sheet.Row()
				if errors.Is(err, io.EOF) {
					break
				}
				require.NoError(t, err)
				res = append(res, row)
			}

			t.Logf("%v", time.Since(start))

			if test.Result != nil {
				require.Equal(t, test.Result, res)
			}
		})
	}
}

func TestOpenFile(t *testing.T) {
	for _, test := range testCases {
		t.Run(test.FileName, func(t *testing.T) {
			start := time.Now()

			doc, err := xlsx.OpenFile("../test/xlsx/" + test.FileName)
			require.NoError(t, err)
			defer doc.Close()

			sheet, err := doc.Sheet(doc.Sheets[0])
			require.NoError(t, err)
			defer sheet.Close()

			res := make([][]any, 0, 100)
			for {
				row, err := sheet.Row()
				if errors.Is(err, io.EOF) {
					break
				}
				require.NoError(t, err)
				res = append(res, row)
			}

			t.Logf("%v", time.Since(start))

			if test.Result != nil {
				require.Equal(t, test.Result, res)
			}
		})
	}
}
