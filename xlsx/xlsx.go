package xlsx

import (
	"archive/zip"
	"bytes"
	"fmt"
)

type XLSX struct {
	Sheets []string

	dateStyles    map[int]bool
	sharedStrings []string
	sheetFiles    map[string]*zip.File
}

type ReadCloser struct {
	XLSX
	closer *zip.ReadCloser
}

func (v *ReadCloser) Close() error {
	return v.closer.Close()
}

func OpenBinary(b []byte) (*XLSX, error) {
	reader, err := zip.NewReader(bytes.NewReader(b), int64(len(b)))
	if err != nil {
		return nil, fmt.Errorf("unable to create new reader: %v", err)
	}

	var v *XLSX
	v, err = open(reader)
	if err != nil {
		return nil, fmt.Errorf("unable to initialise file: %v", err)
	}

	return v, nil
}

func OpenFile(filename string) (*ReadCloser, error) {
	closer, err := zip.OpenReader(filename)
	if err != nil {
		return nil, fmt.Errorf("unable to open file reader: %v", err)
	}

	var v *XLSX
	v, err = open(&closer.Reader)
	if err != nil {
		closer.Close()
		return nil, fmt.Errorf("unable to initialise file: %v", err)
	}

	return &ReadCloser{
		XLSX:   *v,
		closer: closer,
	}, nil
}

func open(zipReader *zip.Reader) (*XLSX, error) {
	var (
		err              error
		sharedStringFile *zip.File
		stylesFile       *zip.File
		workbookFile     *zip.File
		workbookRelsFile *zip.File
	)

	v := &XLSX{}

	for _, file := range zipReader.File {
		switch file.Name {
		case _SharedStringPath:
			sharedStringFile = file
		case _StylesPath:
			stylesFile = file
		case _WorkBookPath:
			workbookFile = file
		case _WorkBookRelsPath:
			workbookRelsFile = file
		}
	}

	if stylesFile == nil {
		return nil, fmt.Errorf("parse file failed: %q not exist", _StylesPath)
	}
	if workbookFile == nil {
		return nil, fmt.Errorf("parse file failed: %q not exist", _WorkBookPath)
	}
	if workbookRelsFile == nil {
		return nil, fmt.Errorf("parse file failed: %q not exist", _WorkBookRelsPath)
	}

	v.sharedStrings, err = parseSharedStrings(sharedStringFile)
	if err != nil {
		return nil, fmt.Errorf("unable to parse shared strings file: %v", err)
	}

	v.dateStyles, err = parseStyles(stylesFile)
	if err != nil {
		return nil, fmt.Errorf("unable to parse styles file: %v", err)
	}

	v.Sheets, v.sheetFiles, err = parseWorkbook(workbookRelsFile, workbookFile, zipReader.File)
	if err != nil {
		return nil, fmt.Errorf("unable to parse worksheets: %v", err)
	}

	return v, nil
}
