package csv

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io"
	"os"
)

type CSV struct {
	reader   *csv.Reader
	finished bool
	row      []any
	err      error
}

type ReadCloser struct {
	closer io.ReadCloser
	CSV
}

func (v *ReadCloser) Close() error {
	return v.closer.Close()
}

func OpenBinary(b []byte, comma ...rune) (*CSV, error) {
	reader := csv.NewReader(bytes.NewReader(b))
	if len(comma) > 0 {
		reader.Comma = comma[0]
	}
	reader.FieldsPerRecord = -1
	reader.ReuseRecord = true
	return &CSV{reader: reader}, nil
}

func OpenFile(filename string, comma ...rune) (*ReadCloser, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("unable to open file reader: %v", err)
	}

	reader := csv.NewReader(file)
	if len(comma) > 0 {
		reader.Comma = comma[0]
	}
	reader.FieldsPerRecord = -1
	reader.ReuseRecord = true

	return &ReadCloser{
		CSV:    CSV{reader: reader},
		closer: file,
	}, nil
}

func (v *CSV) Row() ([]any, error) {
	record, err := v.reader.Read()
	if err != nil {
		return nil, err
	}

	row := make([]any, len(record))
	for idx, val := range record {
		row[idx] = val
	}

	return row, nil
}
